const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//CONNECT DB
//dev_db_url = mongodb://root:root@192.168.99.100:27017/test
let dev_db_url = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`;
//console.log(dev_db_url);
const mongoDB = dev_db_url;
let options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
};

if(process.env.MONGO_OPTION_AUTH_SOURCE) {
    options.authSource = process.env.MONGO_OPTION_AUTH_SOURCE;
}

mongoose.connect(mongoDB, options);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));



app.get('/', (req, res) => {
    res.send("Hello World");
});

//APP
const chatIO = require('./routes/chatio.route');
app.use('/chat', chatIO);

//PRODUCT
const product = require('./routes/product.route');
app.use('/products', product);
//ADD MORE HERE
//

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
io.on('connection', function(socket){
    const sessionID = socket.id;
    console.log('A user connected session id ' + sessionID);
    socket.on('disconnect', function(){
        console.log('User disconnected ' + sessionID);
    });

    // Log message
    socket.on('chat message', function(msg){
        console.log(sessionID + ' message: ' + msg);
        io.emit('chat message', msg);
    });

});


let port = process.env.PORT;
server.listen(port, () => {
    console.log('Welcome CRUD app!');
    console.log(`Server is up an running on port: ${port}`);
});
const express = require('express');
const route = express.Router();

const chatIOController = require('../controllers/chat.controller');

var chatObjController = new chatIOController();

route.get('/', chatObjController.index);

module.exports = route;
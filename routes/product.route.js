const express = require('express');
const router = express.Router();

const productController = require('../controllers/product.controller');

//Route define
router.get('/', productController.index);
router.get('/:id',productController.getDetail);
router.post('/create',productController.productCreate);
router.put('/:id/update',productController.productUpdate);
router.delete('/:id/delete',productController.productDelete);

module.exports = router;

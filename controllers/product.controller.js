const Product = require('../models/product.model');

//Simple version, without validation or sanitation
exports.index = function (req, res) {
    res.send('Greetings from the Index controller!');
};

exports.productCreate = function (req, res) {

    let product = new Product({
        name: req.body.name,
        price: req.body.price
    });

    product.save(function (err) {
        console.log(err);
        res.send('Product Created successfully')
    })
};

exports.getDetail = async (req, res) => {

    // let pr = Product.findById(req.params.id, function ( err ,product) {
    //     console.log(product);
    //     res.send(product);
    // });

   /* pr.then((res) => {
        console.log(res);
    });*/
    //console.log(pr);

    const pr = await Product.findById(req.params.id);
    res.send(pr);
};

exports.productUpdate = async function (req, res) {
    await Product.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if(err){
            console.log(err);
        }
    });

    let pr = await Product.findById(req.params.id);
    let data = {
        msg : "Update successfully",
        data: pr
    };

    res.send(data);
};

exports.productDelete = function (req, res) {
    Product.findByIdAndRemove(req.params.id, function (err) {
        res.send('Deleted successfully!');
    })
};
